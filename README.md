# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Forum-ShareMind
* Key functions (add/delete)
    1. Post Page
    2. Post List
    3. 
    4. 
* Other functions (add/delete)
    1. 
    2. 
    3. 
    4. 

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|N|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y/N|
    算是部分完成吧
## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description
    大多數都是參照Lab_06的方式來做
    在排版上做了一些調整
    在post時加上了post的時間
    加上comment的欄位
    但是作為一個forum
    我沒有完成的東西非常的多
    功能上還有很多的不足
    有更多的時間還是有機會做出的
    
## Security Report (Optional)
